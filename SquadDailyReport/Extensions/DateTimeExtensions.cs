﻿using System;

namespace SquadDailyReport.Extensions
{
    public static class DateTimeExtensions
    {
        public static bool IsWorkingDay(this DateTime date)
        {
            return date.DayOfWeek != DayOfWeek.Saturday
                && date.DayOfWeek != DayOfWeek.Sunday;
        }

        public static int DaysBetween(this DateTime initial, DateTime end)
        {
            var total = 0;
            while (initial.Date <= end.Date)
            {
                if (initial.IsWorkingDay())
                    total++;

                initial = initial.AddDays(1);
            }

            return total;
        }
    }
}