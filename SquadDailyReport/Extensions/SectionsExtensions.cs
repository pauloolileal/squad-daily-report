﻿using System.Collections.Generic;
using System.Linq;
using SquadDailyReport.Helpers;
using SquadDailyReport.Models;

namespace SquadDailyReport.Extensions
{
    public static class SectionsExtensions
    {
        public static string ReplacePendeciesSection(this string html, string chave, List<Pendencia> pendencias)
        {
            pendencias = pendencias.Where(x => !string.IsNullOrEmpty(x.Descricao)).ToList();

            var valor = @"<tr><td align='left' style='padding:0;Margin:0'><table cellpadding='0' cellspacing='0' width='100%' style='mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px'><tbody><tr><td align='center' valign='top' style='padding:0;Margin:0;width:700px'><table cellpadding='0' cellspacing='0' width='100%' role='presentation' style='mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px'><tbody><tr><td align='center' height='10' style='padding:0;Margin:0'></td></tr></tbody></table></td></tr></tbody></table></td></tr>
              <tr><td align='left' bgcolor='#ffffff' style='padding:0;Margin:0;background-color:#ffffff'><table cellpadding='0' cellspacing='0' width='100%' style='mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px'><tbody><tr><td align='center' valign='top' style='padding:0;Margin:0;width:700px'><table cellpadding='0' cellspacing='0' width='100%' bgcolor='#ffffff' style='mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:#ffffff' role='presentation'><tbody><tr><td align='left' style='padding:10px;Margin:0'><h2 style='Margin:0;line-height:36px;mso-line-height-rule:exactly;font-family:Kanit, sans-serif;font-size:30px;font-style:normal;font-weight:bold;color:#023047'>Pendências</h2></td></tr><tr><td style='Margin:0;padding-top:00px;padding-left:10px;padding-right:10px;padding-bottom:20px'><div> @PENDENCIES </div></td></tr></tbody></table></td></tr></tbody></table></td></tr>";

            if (!pendencias.Any())
                valor = string.Empty;

            return html
                .Replace(chave, valor)
                .ReplaceNumber("@PENDENCIES_COUNT", pendencias.Where(x => !x.Resolvido).ToList())
                .ReplacePendenciaStatus("@PENDENCIES_STATUS", pendencias)
                .ReplacePendencias("@PENDENCIES", pendencias);
        }

        public static string ReplaceImpedimentsSection(this string html, string chave, List<Impedimento> impedimentos)
        {
            impedimentos = impedimentos.Where(x => !string.IsNullOrEmpty(x.Estoria)).ToList();

            var valor = @"<tr><td align='left' style='padding:0;Margin:0'><table cellpadding='0' cellspacing='0' width='100%' style='mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px'><tbody><tr><td align='center' valign='top' style='padding:0;Margin:0;width:700px'><table cellpadding='0' cellspacing='0' width='100%' role='presentation' style='mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px'><tbody><tr><td align='center' height='10' style='padding:0;Margin:0'></td></tr></tbody></table></td></tr></tbody></table></td></tr>
                                                <tr><td align='left' bgcolor='#ffffff' style='padding:0;Margin:0;background-color:#ffffff'><table cellpadding='0' cellspacing='0' width='100%' style='mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px'><tbody><tr><td align='center' valign='top' style='padding:0;Margin:0;width:700px'><table cellpadding='0' cellspacing='0' width='100%' bgcolor='#ffffff' style='mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:#ffffff' role='presentation'><tbody><tr><td align='left' style='padding:10px;Margin:0'><h2 style='Margin:0;line-height:36px;mso-line-height-rule:exactly;font-family:Kanit, sans-serif;font-size:30px;font-style:normal;font-weight:bold;color:#023047'>Impedimentos</h2></td></tr><tr><td style='Margin:0;padding-top:0px;padding-left:10px;padding-right:10px;padding-bottom:20px'><div> @IMPEDIMENTS </div></td></tr></tbody></table></td></tr></tbody></table></td></tr>";

            if (!impedimentos.Any() || impedimentos.Any(x => string.IsNullOrEmpty(x.Estoria)))
                valor = string.Empty;

            return html
                .Replace(chave, valor)
                .ReplaceNumber("@IMPEDIMENTS_COUNT", impedimentos)
                .ReplaceImpedimentoStatus("@IMPEDIMENTS_STATUS", impedimentos)
                .ReplaceImpedimentos("@IMPEDIMENTS", impedimentos);
        }

        public static string ReplaceObservationsSection(this string html, string chave, Backlog backlog)
        {
            var valor = @"<tr><td align='left' style='padding:0;Margin:0'><table cellpadding='0' cellspacing='0' width='100%' style='mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px'><tbody><tr><td align='center' valign='top' style='padding:0;Margin:0;width:700px'><table cellpadding='0' cellspacing='0' width='100%' role='presentation' style='mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px'><tbody><tr><td align='center' height='10' style='padding:0;Margin:0'></td></tr></tbody></table></td></tr></tbody></table></td></tr>
                                                <tr><td align='left' bgcolor='#ffffff' style='padding:0;Margin:0;background-color:#ffffff'><table cellpadding='0' cellspacing='0' width='100%' style='mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px'><tbody><tr><td align='center' valign='top' style='padding:0;Margin:0;width:700px'><table cellpadding='0' cellspacing='0' width='100%' bgcolor='#ffffff' style='mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:#ffffff' role='presentation'><tbody><tr><td align='left' style='padding:10px;Margin:0'><h2 style='Margin:0;line-height:36px;mso-line-height-rule:exactly;font-family:Kanit, sans-serif;font-size:30px;font-style:normal;font-weight:bold;color:#023047'>Observações</h2></td></tr><tr><td style='Margin:0;padding-top:0px;padding-left:10px;padding-right:10px;padding-bottom:20px'><p style='Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:arial, 'helvetica neue', helvetica, sans-serif;line-height:27px;color:#333333;font-size:18px'>@OBSERVATIONS</p></td></tr></tbody></table></td></tr></tbody></table></td></tr>";

            if (string.IsNullOrEmpty(backlog.Observacoes))
                valor = string.Empty;

            return html
                .Replace(chave, valor)
                .ReplaceText("@OBSERVATIONS", backlog.Observacoes);
        }

        public static string ReplaceBugsSection(this string html, string chave, List<Bug> bugs)
        {
            bugs = bugs.Where(x => !string.IsNullOrEmpty(x.Descricao)).ToList();

            var valor = @"<tr><td align='left' style='padding:0;Margin:0'><table cellpadding='0' cellspacing='0' width='100%' style='mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px'><tbody><tr><td align='center' valign='top' style='padding:0;Margin:0;width:700px'><table cellpadding='0' cellspacing='0' width='100%' role='presentation' style='mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px'><tbody><tr><td align='center' height='10' style='padding:0;Margin:0'></td></tr></tbody></table></td></tr></tbody></table></td></tr>
              <tr><td align='left' bgcolor='#ffffff' style='padding:0;Margin:0;background-color:#ffffff'><table cellpadding='0' cellspacing='0' width='100%' style='mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px'><tbody><tr><td align='center' valign='top' style='padding:0;Margin:0;width:700px'><table cellpadding='0' cellspacing='0' width='100%' bgcolor='#ffffff' style='mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:#ffffff' role='presentation'><tbody><tr><td align='left' style='padding:10px;Margin:0'><h2 style='Margin:0;line-height:36px;mso-line-height-rule:exactly;font-family:Kanit, sans-serif;font-size:30px;font-style:normal;font-weight:bold;color:#023047'>Bugs Corrigidos</h2></td></tr><tr><td style='Margin:0;padding-top:00px;padding-left:10px;padding-right:10px;padding-bottom:20px'><div> @BUGS </div></td></tr></tbody></table></td></tr></tbody></table></td></tr>";

            if (!bugs.Any())
                valor = string.Empty;

            return html
                .Replace(chave, valor)
                .ReplaceBugs("@BUGS", bugs);
        }
    }
}