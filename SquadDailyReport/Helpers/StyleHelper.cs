﻿using System;
using System.Collections.Generic;

namespace SquadDailyReport.Helpers
{
    public class Style : ICloneable
    {
        public List<string> Itens { get; set; } = new List<string>();

        public Style(string styles)
        {
            Itens.AddRange(styles.Split(";", StringSplitOptions.RemoveEmptyEntries));
        }

        private Style(params string[] styles)
        {
            Itens.AddRange(styles);
        }

        public Style()
        {
        }

        public Style Add(string style)
        {
            var copy = this.Copy();
            copy.Itens.Add(style);
            return copy;
        }

        public Style Add(params string[] styles)
        {
            var copy = this.Copy();
            copy.Itens.AddRange(styles);
            return copy;
        }

        public Style Replace(string key, string value)
        {
            var copy = this.Copy();
            copy.Itens.RemoveAll(x => x.Contains(key));
            copy.Itens.Add($"{key}:{value}");
            return copy;
        }

        public override string ToString()
        {
            var style = $"style='{string.Join("; ", Itens).Trim()}'";
            return style;
        }

        public object Clone()
        {
            return this.MemberwiseClone();
        }

        public Style Copy()
        {
            return new Style(Itens.ToArray());
        }

        public static implicit operator string(Style style) => style.ToString();
    }
}