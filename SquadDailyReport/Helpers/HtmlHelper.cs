﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using SquadDailyReport.Extensions;
using SquadDailyReport.Models;

namespace SquadDailyReport.Helpers
{
    public static class HtmlHelper
    {
        public static string ReplaceToList(this string html, string chave, List<string> dados)
        {
            var list = "";
            foreach (var item in dados)
                list = $"{list}<li>{item}</li>";

            return html.Replace(chave, $"<ul style='margin-block-start: 0;margin-block-end: 0;color: #363636;font-size: 18px;font-family: Poppins,Helvetica,Arial,sans-serif;'>{list}</ul>");
        }

        public static string ReplaceToFormatedDate(this string html, string chave, string date)
        {
            var formatedDate = "-";
            if (!string.IsNullOrEmpty(date))
            {
                var culture = new CultureInfo("pt-BR");

                if (!DateTime.TryParseExact(date, "dd/MM", culture, DateTimeStyles.None, out var data))
                    throw new Exception($"Erro fazer parse da data informada: {date}");

                formatedDate = $"{data.Day} {culture.DateTimeFormat.GetAbbreviatedMonthName(data.Month)}";
            }
            return html.Replace(chave, formatedDate);
        }

        public static string ReplaceIcon(this string html, string chave, bool status)
        {
            var icon = status ? "✔️" : "❌";

            return html.Replace(chave, icon);
        }

        public static string ReplaceNumber<T>(this string html, string chave, List<T> itens)
        {
            return html.Replace(chave, itens.Count.ToString("00"));
        }

        public static string ReplaceNumber(this string html, string chave, int item)
        {
            return html.Replace(chave, item.ToString("00"));
        }

        public static string ReplaceText(this string html, string chave, string text)
        {
            var formatedText = text
                .Replace("\n", "<br>")
                .Replace("@STYLE", "style=\"Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:arial, 'helvetica neue', helvetica, sans-serif;line-height:27px;color:#333333;font-size:18px\"");

            return html.Replace(chave, formatedText);
        }

        public static string ReplacePendenciaStatus(this string html, string chave, List<Pendencia> itens)
        {
            var color = "#407ec9";
            if (!itens.IsPendenciesResolved())
                color = "darkorange";

            return html.Replace(chave, color);
        }

        public static string ReplaceImpedimentoStatus(this string html, string chave, List<Impedimento> itens)
        {
            var color = "#407ec9";
            if (!itens.IsImpedimentsResolved())
                color = "indianred";

            return html.Replace(chave, color);
        }

        public static string ReplaceTableEstorias(this string html, string chave, List<Estoria> estorias)
        {
            estorias = estorias.Where(x => !string.IsNullOrEmpty(x.Demanda)).ToList();

            var tdStyle = new Style("border-bottom: 1px solid #ddd;padding: 5px;word-break: break-word;text-align: left;");
            var thStyle = new Style("text-align: left;background-color: lightgray;padding:8px");

            var itens = new StringBuilder();
            foreach (var item in estorias)
            {
                itens
                    .AppendLine("<tr>")
                    .AppendFormat("<td {0}>{1}</td>", tdStyle, item.Demanda)
                    .AppendFormat("<td {0}>", tdStyle);

                foreach (var desenvolvedor in item.Desenvolvedores)
                {
                    itens.Append(desenvolvedor);
                    if (item.Desenvolvedores.IndexOf(desenvolvedor) < item.Desenvolvedores.Count)
                        itens.Append("<br>");
                }

                itens
                    .Append("</td>")
                    .AppendFormat("<td {0}text-align:right;padding-right:8px;'>{1}%</td>", tdStyle.Replace("text-align", "right"), item.GetProgresso())
                    .AppendLine("</tr>");
            }

            var table = new StringBuilder()
                        .AppendLine("<table style='width: 100%; border-spacing: 1px; color: #363636; font-size: 15px; font-family: Poppins,Helvetica,Arial,sans-serif;'>")
                        .AppendLine("<thead>")
                        .AppendLine("<tr>")
                        .AppendLine($"<th {thStyle}>Demanda</th>")
                        .AppendLine($"<th {thStyle}>Desenvolvedor</th>")
                        .AppendLine($"<th {thStyle}>Progresso</th>")
                        .AppendLine("</tr>")
                        .AppendLine("</thead>")
                        .AppendLine("<tbody>")
                        .AppendLine(itens.ToString())
                        .AppendLine("</tbody>")
                        .AppendLine("</table>");

            return html.Replace(chave, table.ToString());
        }

        public static string ReplacePendencias(this string html, string chave, List<Pendencia> dados)
        {
            var pendencias = string.Empty;
            if (dados.Any())
            {
                var tdStyle = new Style("border-bottom: 1px solid #ddd;padding: 5px;text-align: left;");
                var thStyle = new Style("text-align: left;background-color: lightgray;padding:8px");

                var itens = new StringBuilder();
                foreach (var item in dados)
                {
                    itens
                        .AppendLine("<tr>")
                        .AppendFormat("<td {0}>{1}</td>", tdStyle.Add("word-break: break-word"), item.Descricao)
                        .AppendFormat("<td {0}>", tdStyle.Add("width: 10%"));

                    foreach (var responsavel in item.Responsaveis)
                    {
                        itens.Append(responsavel);
                        if (item.Responsaveis.IndexOf(responsavel) < item.Responsaveis.Count)
                            itens.Append("<br>");
                    }

                    var icon = item.Resolvido ? "✔️" : "❌";

                    itens
                        .Append("</td>")
                        .AppendFormat("<td {0}>{1}</td>", tdStyle.Add("padding-right:8px").Add("width: 10%"), item.Data)
                        .AppendFormat("<td {0}>{1}</td>", tdStyle.Replace("text-align", "center").Add("width: 5%"), icon)
                        .AppendLine("</tr>");
                }

                var table = new StringBuilder()
                            .AppendLine("<table style='width: 100%; border-spacing: 1px; color: #363636; font-size: 15px; font-family: Poppins,Helvetica,Arial,sans-serif;'>")
                            .AppendLine("<thead>")
                            .AppendLine("<tr>")
                            .AppendLine($"<th {thStyle}>Descrição</th>")
                            .AppendLine($"<th {thStyle}>Responsável</th>")
                            .AppendLine($"<th {thStyle}>Data</th>")
                            .AppendLine($"<th {thStyle}>Status</th>")
                            .AppendLine("</tr>")
                            .AppendLine("</thead>")
                            .AppendLine("<tbody>")
                            .AppendLine(itens.ToString())
                            .AppendLine("</tbody>")
                            .AppendLine("</table>");

                pendencias = $"<br><span style='color:#363636;font-size:18px;font-family:Poppins,Helvetica,Arial,sans-serif'>{table.ToString()}</span><br>";
            }
            return html.Replace(chave, pendencias);
        }

        public static string ReplaceImpedimentos(this string html, string chave, List<Impedimento> dados)
        {
            var impedimentos = string.Empty;
            if (dados.Any())
            {
                var tdStyle = new Style("border-bottom: 1px solid #ddd;padding: 5px;text-align: left;");
                var thStyle = new Style("text-align: left;background-color: lightgray;padding:8px");

                var itens = new StringBuilder();
                foreach (var item in dados)
                {
                    itens
                        .AppendLine("<tr>")
                        .AppendFormat("<td {0}>{1}</td>", tdStyle.Add("word-break: break-word"), item.Estoria)
                        .AppendFormat("<td {0}>{1}</td>", tdStyle.Add("word-break: break-word"), item.Motivo)
                        .AppendFormat("<td {0}>{1}</td>", tdStyle.Add("width: 10%"), item.Data)
                        .AppendLine("</tr>");
                }

                var table = new StringBuilder()
                            .AppendLine("<table style='width: 100%; border-spacing: 1px; color: #363636; font-size: 15px; font-family: Poppins,Helvetica,Arial,sans-serif;'>")
                            .AppendLine("<thead>")
                            .AppendLine("<tr>")
                            .AppendLine($"<th {thStyle}>Estória</th>")
                            .AppendLine($"<th {thStyle}>Motivo</th>")
                            .AppendLine($"<th {thStyle}>Data</th>")
                            .AppendLine("</tr>")
                            .AppendLine("</thead>")
                            .AppendLine("<tbody>")
                            .AppendLine(itens.ToString())
                            .AppendLine("</tbody>")
                            .AppendLine("</table>");

                impedimentos = $"<br><span style='color:#363636;font-size:18px;font-family:Poppins,Helvetica,Arial,sans-serif'>{table.ToString()}</span><br>";
            }
            return html.Replace(chave, impedimentos);
        }

        public static string ReplaceImage(this string html, string chave, string path)
        {
            var htmlImage = "";
            if (File.Exists(path))
            {
                var image = File.ReadAllBytes(path);
                var base64 = Convert.ToBase64String(image);
                htmlImage = $"data:image/png;base64, {base64}";
            }
            else
            {
                Console.WriteLine("Arquivo de burndown não existe");
            }

            return html.Replace(chave, htmlImage);
        }

        public static string ReplaceIconStatus(this string html, string chave, Backlog backlog)
        {
            if (!backlog.IsPendenciesResolved())
                backlog.Status = Backlog.StatusType.WARNING;

            if (!backlog.IsImpedimentsResolved())
                backlog.Status = Backlog.StatusType.DANGER;

            var icon = backlog.Status switch
            {
                Backlog.StatusType.WARNING => "❕",
                Backlog.StatusType.DANGER => "✖",
                _ => "✓",
            };

            var color = backlog.Status switch
            {
                Backlog.StatusType.WARNING => "darkorange",
                Backlog.StatusType.DANGER => "indianred",
                _ => "mediumseagreen",
            };

            var section = $"<span style='float: right;margin: 9px;width: 35px;height: 35px;line-height: 35px;border-radius: 50%;font-size: 25px;color: white;text-align: center;background: {color};'>{icon}</span>";


            return html.Replace(chave, section);
        }

        public static string ReplaceBanner(this string html, string chave, Backlog backlog)
        {
            var color = backlog.Status switch
            {
                Backlog.StatusType.WARNING => "#ffae00",
                Backlog.StatusType.DANGER => "#ec5840",
                _ => "mediumseagreen",
            };

            var banner = new StringBuilder()
                .AppendLine("<table class='mcnBoxedTextBlock' style='min-width:100%' width='100%' cellspacing='0' cellpadding='0' border='0'><!--[if gte mso 9]>")
                .AppendLine("<table align='center' border='0' cellspacing='0' cellpadding='0' width='100%'>")
                .AppendLine("<![endif]--><tbody class='mcnBoxedTextBlockOuter'><tr><td class='mcnBoxedTextBlockInner' valign='top'><!--[if gte mso 9]>")
                .AppendLine("<td align='center' valign='top'>")
                .AppendLine($"<![endif]--><table style='min-width:100%' class='mcnBoxedTextContentContainer' width='100%' cellspacing='0' cellpadding='0' border='0' align='left'><tbody><tr><td style='padding-top:8px;padding-bottom:16px'><table class='mcnTextContentContainer' style='background-color:{color}' width='100%' cellspacing='0' border='0'><tbody><tr><td class='mcnTextContent' style='padding:16px;text-align:center' valign='top'><span style='color:#fff;font-size:14px;font-weight:600;font-family:Poppins,Helvetica,Arial,sans-serif'>{backlog.Observacoes}</span></td></tr></tbody></table></td></tr></tbody></table><!--[if gte mso 9]>")
                .AppendLine("</td>")
                .AppendLine("<![endif]--><!--[if gte mso 9]>")
                .AppendLine("</tr>")
                .AppendLine("</table>")
                .AppendLine("<![endif]--></td></tr></tbody></table>");

            return html.Replace(chave, banner.ToString());
        }

        public static string ReplaceBugs(this string html, string chave, List<Bug> dados)
        {
            var bugs = string.Empty;
            dados = dados.Where(x => !string.IsNullOrEmpty(x.Descricao)).ToList();

            if (dados.Any())
            {
                var tdStyle = new Style("border-bottom: 1px solid #ddd;padding: 5px;text-align: left;");
                var thStyle = new Style("text-align: left;background-color: lightgray;padding:8px");

                var itens = new StringBuilder();
                foreach (var item in dados)
                {
                    itens
                        .AppendLine("<tr>")
                        .AppendFormat("<td {0}>{1}</td>", tdStyle.Add("word-break: break-word").Add("width: 70%"), item.Descricao)
                        .AppendFormat("<td {0}>{1}</td>", tdStyle, item.Desenvolvedor)
                        .AppendLine("</tr>");
                }

                var table = new StringBuilder()
                            .AppendLine("<table style='width: 100%; border-spacing: 1px; color: #363636; font-size: 15px; font-family: Poppins,Helvetica,Arial,sans-serif;'>")
                            .AppendLine("<thead>")
                            .AppendLine("<tr>")
                            .AppendLine($"<th {thStyle}>Descrição</th>")
                            .AppendLine($"<th {thStyle}>Desenvolvedor</th>")
                            .AppendLine("</tr>")
                            .AppendLine("</thead>")
                            .AppendLine("<tbody>")
                            .AppendLine(itens.ToString())
                            .AppendLine("</tbody>")
                            .AppendLine("</table>");

                bugs = $"<br><span style='color:#363636;font-size:18px;font-family:Poppins,Helvetica,Arial,sans-serif'>{table.ToString()}</span><br>";
            }

            return html.Replace(chave, bugs);
        }
    }
}