﻿using System;
using Newtonsoft.Json;

namespace SquadDailyReport.Converters
{
    public class BooleanPortugueseConverter : JsonConverter
    {
        public override bool CanWrite { get { return true; } }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            if (value is bool data)
                if (data)
                {
                    writer.WriteValue("sim");
                }
                else
                    writer.WriteValue("não");
            else
                writer.WriteValue(value);
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            var value = reader.Value;

            if (value != null && ("sim".Equals(value.ToString(), StringComparison.OrdinalIgnoreCase) || "true".Equals(value.ToString(), StringComparison.OrdinalIgnoreCase)))
                return true;

            return false;
        }

        public override bool CanConvert(Type objectType)
        {
            if (objectType == typeof(String) || objectType == typeof(Boolean))
                return true;

            return false;
        }
    }
}