﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using McMaster.Extensions.CommandLineUtils;
using Newtonsoft.Json;
using SquadDailyReport.Converters;
using SquadDailyReport.Models;

namespace SquadDailyReport.Commands
{
    [Command("settings", "s")]
    public class SettingsCommand
    {
        [Option(CommandOptionType.NoValue, Description = "Forçar resetar o arquivo de configurações", Template = "--force")]
        public bool Force { get; set; }

        public int OnExecute()
        {
            var path = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "DailyReports", "settings.json");
            var settings = new Settings();
            if (File.Exists(path))
                settings = JsonConvert.DeserializeObject<Settings>(File.ReadAllText(path));

            CompleteSettings(ref settings);

            var data = JsonConvert.SerializeObject(settings, Formatting.Indented, new BooleanPortugueseConverter());
            File.WriteAllText(path, data);

            Process.Start(@"cmd.exe ", @"/c " + path);

            return 1;
        }

        private static void CompleteSettings(ref Settings settings)
        {
            if (!settings.Backlog.Estorias.Any()) settings.Backlog.Estorias.Add(new());
            if (!settings.Backlog.Pendencias.Any()) settings.Backlog.Pendencias.Add(new());
            if (!settings.Backlog.Impedimentos.Any()) settings.Backlog.Impedimentos.Add(new());
            if (!settings.Backlog.Bugs.Any()) settings.Backlog.Bugs.Add(new());

            if (settings.Backlog.Estorias.Any())
            {
                foreach (var estoria in settings.Backlog.Estorias.Where(x => !x.Desenvolvedores.Any()))
                {
                    if (estoria.Desenvolvedores == null)
                        estoria.Desenvolvedores = new List<string>();

                    if (!estoria.Desenvolvedores.Any())
                        estoria.Desenvolvedores.Add(string.Empty);
                }
            }

            if (settings.Backlog.Pendencias.Any())
            {
                foreach (var pendencia in settings.Backlog.Pendencias.Where(x => !x.Responsaveis.Any()))
                {
                    if (pendencia.Responsaveis == null)
                        pendencia.Responsaveis = new List<string>();

                    if (!pendencia.Responsaveis.Any())
                        pendencia.Responsaveis.Add(string.Empty);
                }
            }
        }
    }
}