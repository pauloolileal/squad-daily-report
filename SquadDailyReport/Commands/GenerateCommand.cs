﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using McMaster.Extensions.CommandLineUtils;
using Newtonsoft.Json;
using SquadDailyReport.Extensions;
using SquadDailyReport.Helpers;
using SquadDailyReport.Models;

namespace SquadDailyReport.Commands
{
    [Command("generate", "g")]
    public class GenerateCommand
    {
        private Settings _settings { get; set; }

        [Option(CommandOptionType.SingleValue, Description = "Especificar o caminho do json", Template = "-fj|--from-json")]
        public string JsonPath { get; set; }

        [Option(CommandOptionType.SingleValue, Description = "Especificar o caminho do burndown", Template = "-b|--burndows")]
        public string BurndownPath { get; set; }

        [Option(CommandOptionType.NoValue, Description = "Impedir de abrir arquivo depois da geração", Template = "-no|--no-open")]
        public bool NotOpen { get; set; } = false;

        private void SetSettings()
        {
            var rootPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "DailyReports");
            var path = Path.Combine(rootPath, "settings.json");
            if (string.IsNullOrEmpty(JsonPath) && Directory.Exists(JsonPath))
                path = JsonPath;

            _settings = JsonConvert.DeserializeObject<Settings>(File.ReadAllText(path));

            if (!string.IsNullOrEmpty(BurndownPath))
                _settings.Burndown = BurndownPath;
            else
            {
                var files = Directory.GetFiles(rootPath, "burndown.*");
                if (files.Any())
                    _settings.Burndown = files.First();
            }
        }

        public int OnExecute()
        {
            SetSettings();
            var file = File.ReadAllText(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Templates", "Template.html"))
                .Replace("@DATE", DateTime.Now.ToString("dddd - dd 'de' MMMM"))
                .Replace("@SQUAD_NAME", _settings.Squad.Name)
                .ReplaceIconStatus("@ICON_STATUS", _settings.Backlog)
                .ReplaceObservationsSection("@OBSERVATIONS_SECTION", _settings.Backlog)
                .ReplaceNumber("@SPRINT_NUMBER", _settings.Sprint.Atual)
                .ReplaceToList("@OBJECTIVES", _settings.Squad.Objetivos)
                .ReplaceNumber("@SPRINT_TIME", CalculateLeftDays(_settings.Sprint.Fim, _settings.Sprint.Inicio, false))
                .ReplaceNumber("@SPRINT_STORY", _settings.Backlog.Estorias.Count)
                .ReplaceNumber("@SPRINT_POINTS", _settings.Backlog.Estorias.Sum(x => x.Pontos))
                .ReplaceNumber("@SPRINT_LEFT", CalculateLeftDays(_settings.Sprint.Fim, _settings.Sprint.Inicio, true))
                .ReplaceNumber("@WIP_COUNT", _settings.Backlog.WIPs)
                .ReplacePendeciesSection("@PENDECIES_SECTION", _settings.Backlog.Pendencias)
                .ReplaceImpedimentsSection("@IMPEDIMENTS_SECTION", _settings.Backlog.Impedimentos)
                .ReplaceBugsSection("@BUGS_SECTION", _settings.Backlog.Bugs)
                .ReplaceTableEstorias("@TASKBOARD", _settings.Backlog.Estorias)
                .ReplaceToFormatedDate("@PLANNING", _settings.Ritos.Planning)
                .ReplaceToFormatedDate("@REVIEW", _settings.Ritos.Review)
                .ReplaceNumber("@STOCK", _settings.Sprint.Estoque)
                .ReplaceNumber("@STAGING", _settings.Sprint.Homologacao)
                .ReplaceNumber("@PLANNED_STORIES", _settings.Backlog.Estorias.Count)
                .ReplaceNumber("@PLANNED_POINTS", _settings.Backlog.Estorias.Sum(x => x.Pontos))
                .ReplaceNumber("@FINISHED_STORIES", _settings.Backlog.GetEstoriasCompleted())
                .ReplaceNumber("@LEFT_STORIES", _settings.Backlog.GetEstoriasPending())
                .Replace("@BOARD_LINK", _settings.Squad.Url)
                .ReplaceImage("@BURNDOWN", _settings.Burndown)
                ;

            var filename = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "DailyReports", "Reports", $"report-{_settings.Squad.Name.ToLower().Replace(" ", "-")}-{DateTime.Now.ToString("dd-MM-yyyy")}.html");

            var directory = new FileInfo(filename).Directory;
            if (!directory.Exists)
                directory.Create();

            File.WriteAllText(filename, file);
            if (!NotOpen)
                Process.Start(@"cmd.exe ", @"/c " + filename);

            return 1;
        }

        private static int CalculateLeftDays(string endDate, string initialDate, bool updateDaily)
        {
            var start = DateTime.Now.AddDays(1);
            var initial = DateTime.Parse(initialDate);

            if ((updateDaily && start < initial) || !updateDaily)
                start = initial;

            var end = DateTime.Parse(endDate);
            return start.DaysBetween(end);
        }
    }
}