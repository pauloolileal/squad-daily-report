﻿using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using SquadDailyReport.Extensions;

namespace SquadDailyReport.Models
{
    public class Backlog
    {
        public StatusType Status { get; set; }
        public string Observacoes { get; set; }
        public List<Estoria> Estorias { get; set; }
        public List<Pendencia> Pendencias { get; set; }
        public List<Impedimento> Impedimentos { get; set; }
        public List<Bug> Bugs { get; set; }
        public int WIPs { get; set; }

        public Backlog()
        {
            Status = StatusType.SUCCESS;
            Estorias = new List<Estoria>();
            Pendencias = new List<Pendencia>();
            Impedimentos = new List<Impedimento>();
            Bugs = new List<Bug>();

            Observacoes = string.Empty;
        }

        [JsonConverter(typeof(StringEnumConverter))]
        public enum StatusType
        {
            [EnumMember(Value = "SUCESSO")]
            SUCCESS,

            [EnumMember(Value = "ATENÇÃO")]
            WARNING,

            [EnumMember(Value = "PERIGO")]
            DANGER
        }

        public int GetEstoriasPending()
        {
            return Estorias.Count(x =>
                x.Progresso.HasValue && x.Progresso < 100 ||
                x.TempoRestante.HasValue && x.TempoRestante > 0
                );
        }

        public int GetEstoriasCompleted()
        {
            return Estorias.Count(x =>
                x.Progresso.HasValue && x.Progresso == 100 ||
                x.TempoRestante.HasValue && x.TempoRestante == 0
                );
        }

        public bool IsPendenciesResolved() => Pendencias.IsPendenciesResolved();

        public bool IsImpedimentsResolved() => Impedimentos.IsImpedimentsResolved();
    }
}