﻿using System;
using System.Collections.Generic;

namespace SquadDailyReport.Models
{
    public class Estoria
    {
        public string Demanda { get; set; }
        public List<string> Desenvolvedores { get; set; }
        public double? Progresso { get; set; }
        public double? TempoRestante { get; set; }
        public int Pontos { get; set; }

        public Estoria()
        {
            TempoRestante = 0;
            Progresso = 0;
            Demanda = string.Empty;
            Desenvolvedores = new List<string>();
        }

        public double GetProgresso()
        {
            if (Progresso.HasValue)
                return Progresso.Value;
            else if (TempoRestante.HasValue)
            {
                var tempoTotal = Pontos * 8;

                var tempoCumprido = tempoTotal - TempoRestante.Value;

                var porcentagemCumprida = (100 * tempoCumprido) / tempoTotal;

                return Math.Round(porcentagemCumprida / 5) * 5;
            }

            return 0;
        }
    }
}