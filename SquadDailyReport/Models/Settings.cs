﻿namespace SquadDailyReport.Models
{
    public class Settings
    {
        public Squad Squad { get; set; }
        public Sprint Sprint { get; set; }
        public Ritos Ritos { get; set; }
        public Backlog Backlog { get; set; }
        public string Burndown { get; set; }

        public Settings()
        {
            Squad = new();
            Sprint = new();
            Ritos = new();
            Backlog = new();
        }
    }
}