﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using SquadDailyReport.Converters;

namespace SquadDailyReport.Models
{
    public class Pendencia
    {
        public string Descricao { get; set; }
        public List<string> Responsaveis { get; set; }
        public string Data { get; set; }

        [JsonConverter(typeof(BooleanPortugueseConverter))]
        public bool Resolvido { get; set; }

        public Pendencia()
        {
            Data = DateTime.Now.ToString("dd/MM/yyyy");
            Responsaveis = new List<string>();
            Descricao = string.Empty;
        }
    }
}