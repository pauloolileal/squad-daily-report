﻿using System.Collections.Generic;

namespace SquadDailyReport.Models
{
    public class Squad
    {
        public string Name { get; set; }
        public string Url { get; set; }
        public List<string> Objetivos { get; set; }

        public Squad()
        {
            Objetivos = new List<string>();
            Name = Url = string.Empty;
        }
    }
}