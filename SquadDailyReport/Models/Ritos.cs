﻿using System;

namespace SquadDailyReport.Models
{
    public class Ritos
    {
        public string Planning { get; set; }
        public string Review { get; set; }

        public Ritos()
        {
            Planning = DateTime.Now.ToString("dd/MM");
            Review = DateTime.Now.ToString("dd/MM");
        }
    }
}